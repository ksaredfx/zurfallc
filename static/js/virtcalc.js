var baseband
var baseram
var basedisk

$ram = 0;
$storage = 0;
$bandwidth = 0;

function calcData() {

  $ramCost = $ram * 10;
  $storageCost = $storage;
  $bandwidthCost = $bandwidth / 20;

  $totalCost = $ramCost + $storageCost + $bandwidthCost;

  $diskIncluded = $totalCost;
  $bandIncluded = $totalCost * 20;

  $diskTotal = $storage + $diskIncluded;
  $bandTotal = $bandwidth + $bandIncluded;

  if ($ramCost>=5)
    {
    $(".totals").show();
    $(".nothing").hide();

    $("#incluDisk").html($diskIncluded);
    $("#incluBand").html($bandIncluded);

    $("#totalRAM").html($ram);
    $("#totalCost").html($totalCost);
    $("#totalDisk").html($diskTotal);
    $("#totalBand").html($bandTotal);
    }
    else
    {
    $(".nothing").show();
    $(".totals").hide();
  };
}
  
$("#ram").each(function () {
  $("<div>").addClass("output").insertAfter($(this));
})
  .bind("slider:ready slider:changed", function (event, data) {
    $(this).nextAll(".output:first").html(data.value.toFixed(1) + "Gb RAM");
    $ram = data.value;
    calcData();
});
$("#storage").each(function () {
  $("<div>").addClass("output").insertAfter($(this));
})
  .bind("slider:ready slider:changed", function (event, data) {
    $storage = data.value;
    $(this).nextAll(".output:first").html(data.value.toFixed(1) + "Gb Storage");
    calcData();
});
$("#bandwidth").each(function () {
  $("<div>").addClass("output").insertAfter($(this));
})
  .bind("slider:ready slider:changed", function (event, data) {
    $bandwidth = data.value;
    $(this).nextAll(".output:first").html(data.value.toFixed(1) + "Gb Bandwidth");
    calcData();
});