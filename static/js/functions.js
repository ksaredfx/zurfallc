$(window).load(function() {
  $(".showcase a").each(function(){
    $(this).css({opacity: 0.01}).hover(function(){
      $(this).stop().animate({opacity: 1});
    },function(){
      $(this).stop().animate({opacity: 0.01});
    });    
  });
});

$(window).scroll(function() {
  var scrollCenter = $(window).height() / 2;
	$(".specific").each(function(){
	    var workOffsetTop = $(this).offset().top - $(document).scrollTop();
	    var workOffsetBot = workOffsetTop + $(this).height();
	    
	    if ( workOffsetTop > scrollCenter ) {
	      $(this).stop().animate({opacity: 0.20});
	    } else if ( workOffsetBot < scrollCenter ) {
	      $(this).stop().animate({opacity: 0.20});
	    } else {
	      $(this).stop().animate({opacity: 1});
	    };
	  });
});